package com.javarush.test.level06.lesson11.bonus02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Нужно добавить в программу новую функциональность
Задача: У каждой кошки есть имя и кошка-мама. Создать класс, который бы описывал данную ситуацию. Создать два объекта: кошку-дочь и кошку-маму. Вывести их на экран.
Новая задача: У каждой кошки есть имя, кошка-папа и кошка-мама. Изменить класс Cat так, чтобы он мог описать данную ситуацию.
Создать 6 объектов: маму, папу, сына, дочь, бабушку(мамина мама) и дедушку(папин папа).
Вывести их всех на экран в порядке: дедушка, бабушка, папа, мама, сын, дочь.

Пример ввода:
дедушка Вася
бабушка Мурка
папа Котофей
мама Василиса
сын Мурчик
дочь Пушинка

Пример вывода:
Cat name is дедушка Вася, no mother, no father
Cat name is бабушка Мурка, no mother, no father
Cat name is папа Котофей, no mother, father is дедушка Вася
Cat name is мама Василиса, mother is бабушка Мурка, no father
Cat name is сын Мурчик, mother is мама Василиса, father is папа Котофей
Cat name is дочь Пушинка, mother is мама Василиса, father is папа Котофей
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String gfName = reader.readLine();
        Cat gfFather = new Cat(gfName);       // кот дедушка
        String gmName = reader.readLine();
        Cat gmMother = new Cat(gmName);      // кошка бабушка

        String fatherName = reader.readLine();
        Cat catFather = new Cat(fatherName, gfFather, null);    // кот папа

        String motherName = reader.readLine(); // кошка мама
        Cat catMother = new Cat(motherName, null, gmMother);

        String sName = reader.readLine(); // кот сын
        Cat catS = new Cat(sName, catFather, catMother);

        String dName = reader.readLine(); // кошка дочь
        Cat catD = new Cat(dName, catFather, catMother);

        System.out.println(gfFather);
        System.out.println(gmMother);
        System.out.println(catFather);
        System.out.println(catMother);
        System.out.println(catS);
        System.out.println(catD);
    }

    public static class Cat
    {
        private String name;
        private Cat parentMother;
        private Cat parentFather;

        Cat(String name)
        {
            this.name = name;
        }

        Cat(String name, Cat parentFather, Cat parentMother)    // собственное имя и родители
        {
            this.name = name;
            this.parentFather = parentFather;
            this.parentMother = parentMother;
        }

        @Override
        public String toString()
        {
            if (parentMother == null && parentFather == null)      // & - для проверки обоих условий, а не только первого
                return "Cat name is " + name + ", no mother, no father";
            else if(parentMother == null)
                return "Cat name is " + name + ", no mother, father is " + parentFather.name;
            else if(parentFather == null)
                return "Cat name is " + name + ", mother is " + parentMother.name + ", no father";
            else
                return "Cat name is " + name + ", mother is " + parentMother.name + ", father is " + parentFather.name;
        }
    }
}
