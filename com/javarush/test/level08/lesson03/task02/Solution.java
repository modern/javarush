package com.javarush.test.level08.lesson03.task02;

/* HashMap из 10 пар
Создать коллекцию HashMap<String, String>, занести туда 10 пар строк:
арбуз – ягода, банан – трава, вишня – ягода, груша – фрукт, дыня – овощ, ежевика – куст, жень-шень – корень, земляника – ягода, ирис – цветок, картофель – клубень.
Вывести содержимое коллекции на экран, каждый элемент с новой строки.
Пример вывода (тут показана только одна строка):
картофель – овощ
*/

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        HashMap<String, String> veg = new HashMap<String, String>();

        veg.put("арбуз","ягода");
        veg.put("банан","трава");
        veg.put("вишня","ягода");
        veg.put("груша","фрукт");
        veg.put("дыня","овощ");
        veg.put("ежевика","куст");
        veg.put("жень-шень","корень");
        veg.put("земляника","ягода");
        veg.put("ирис","цветок");
        veg.put("картофель","клубень");

        Set<Map.Entry<String, String>> set = veg.entrySet();

        for (Map.Entry<String, String> me : set) {
            System.out.print(me.getKey());
            System.out.print(" - ");
            System.out.print(me.getValue());
            System.out.println();
        }
    }
}
