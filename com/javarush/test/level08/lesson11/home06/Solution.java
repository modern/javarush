package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{

    public static void main(String[] args)
    {
        Human ch1 = new Human();
        ch1.age = 20;
        ch1.name = "ch1";
        ch1.sex  = true;

        Human ch2 = new Human();
        ch2.age = 20;
        ch2.name = "ch2";
        ch2.sex  = true;

        Human ch3 = new Human();
        ch3.age = 20;
        ch3.name = "ch3";
        ch3.sex  = true;

        Human father = new Human();
        father.age = 20;
        father.children.add(ch1);
        father.children.add(ch2);
        father.children.add(ch3);
        father.name = "Don";
        father.sex  = true;

        Human mother = new Human();
        mother.age = 20;
        mother.children.add(ch1);
        mother.children.add(ch2);
        mother.children.add(ch3);
        mother.name = "Dona";
        mother.sex  = false;

        Human gf1 = new Human();
        gf1.age = 67;
        gf1.children.add(father);
        gf1.name = "Mike";
        gf1.sex  = true;

        Human gf2 = new Human();
        gf2.age = 77;
        gf2.children.add(mother);
        gf2.name = "Ive";
        gf2.sex  = true;

        Human gm1 = new Human();
        gm1.age = 47;
        gm1.children.add(father);
        gm1.name = "Madonna";
        gm1.sex  = false;

        Human gm2 = new Human();
        gm2.age = 27;
        gm2.children.add(mother);
        gm2.name = "Maria";
        gm2.sex  = false;

        System.out.println(gf1.toString());
        System.out.println(gf2.toString());
        System.out.println(gm1.toString());
        System.out.println(gm2.toString());

        System.out.println(father.toString());
        System.out.println(mother.toString());

        System.out.println(ch1.toString());
        System.out.println(ch2.toString());
        System.out.println(ch3.toString());


    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children = new ArrayList<Human>();

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
