package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Пример ввода:
мама     мыла раму.
Пример вывода:
Мама     Мыла Раму.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String mes = reader.readLine();
        StringBuffer toUpper = new StringBuffer(mes);

        String first = String.valueOf(mes.charAt(0));
        toUpper.replace(0, 1, first.toUpperCase());

        for (int i = 0; i < mes.length()-1; i++) {
            String tmp = String.valueOf(mes.charAt(i));

            if (tmp.equals(" ")) {
                String r = String.valueOf(mes.charAt(i+1));
                toUpper.replace(i+1, i+2, r.toUpperCase());
            }
        }
        System.out.println(toUpper);
    }
}
