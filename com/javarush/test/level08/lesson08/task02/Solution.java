package com.javarush.test.level08.lesson08.task02;

import java.util.*;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        HashSet<Integer> num = new HashSet<Integer>();
        for(int i = 0; i < 20; i++) {
            num.add(i);
        }

        return num;

    }

    public static HashSet<Integer> removeAllNumbersMoreThen10(HashSet<Integer> set)
    {
        List<Integer> keys = new ArrayList<Integer>();

        for (Integer entry : set) {
            if(entry > 10) {
                keys.add(entry);
            }
        }

        for(Integer key : keys) {
            set.remove(key);
        }
        return set;
    }

    public static void main(String[] args)
    {
        System.out.println(removeAllNumbersMoreThen10(createSet()));
    }
}
