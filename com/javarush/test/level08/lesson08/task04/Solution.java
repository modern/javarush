package com.javarush.test.level08.lesson08.task04;

import java.util.*;

/* Удалить всех людей родившихся летом
Создать словарь (Map<String, Date>), и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Сталлоне", new Date("JULY 1 1980"));
        map.put("ййй", new Date("JUNE 1 1980"));
        map.put("цц", new Date("JUNE 1 1980"));
        map.put("уу", new Date("JUNE 1 1980"));
        map.put("кк", new Date("JUNE 1 1980"));
        map.put("ее", new Date("JUNE 1 1980"));
        map.put("нн", new Date("SEP 1 1980"));
        map.put("гг", new Date("JUNE 1 1980"));
        map.put("рр", new Date("JUNE 1 1980"));
        map.put("вв", new Date("JUNE 1 1980"));

        return map;

    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        List<String> keys = new ArrayList<String>();

        for (Map.Entry<String, Date> entry : map.entrySet()) {
            if(entry.getValue().getMonth() == 5 || entry.getValue().getMonth() == 6 || entry.getValue().getMonth() == 7) {
                keys.add(entry.getKey());
            }
        }

        for(String key : keys) {
            map.remove(key);
        }
    }

}
