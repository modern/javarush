package com.javarush.test.level08.lesson08.task05;

import java.util.*;

public class RemoveSummer
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Сталлоне", new Date("JULY 1 1980"));
        map.put("Кунис",    new Date("JUNE 1 1980"));
        map.put("Стетхем",  new Date("SEP 1 1980"));

        return map;

    }

    public static Map<String, Date> removeAllSummerPeople(HashMap<String, Date> map)
    {
        //Map<String, Date> summerBorn = new HashMap<String, Date>();
        List<String> keys = new ArrayList<String>();

        for (Map.Entry<String, Date> entry : map.entrySet()) {
            if(entry.getValue().getMonth() == 5 || entry.getValue().getMonth() == 6 || entry.getValue().getMonth() == 7) {
                keys.add(entry.getKey());
            }
        }

        for(String key : keys) {
            map.remove(key);
        }

        return map;
    }

    public static void main(String[] args) throws Exception {
        //Date d1 = new Date("JUN 1 1980");
        //Date d2 = new Date("JULY 1 1980");
        //Date d3 = new Date("AUGUST 1 1980");

        //System.out.println(d1.getMonth());
        //System.out.println(d2.getMonth());
        //System.out.println(d3.getMonth());
        System.out.println(removeAllSummerPeople(createMap()));
    }
}
