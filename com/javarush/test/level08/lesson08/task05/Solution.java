package com.javarush.test.level08.lesson08.task05;

import java.util.*;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> people = new HashMap<String, String>();

        people.put("арбуз","ягода");
        people.put("банан","трава");
        people.put("вишня","ягода");
        people.put("груша","фрукт");
        people.put("дыня","овощ");
        people.put("ежевика","куст");
        people.put("жень-шень","корень");
        people.put("земляника","ягода");
        people.put("ирис","цветок");
        people.put("картофель","клубень");

        return people;
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        Map<String, String> duplicates = new HashMap<String, String>();
        List<String> keys = new ArrayList<String>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (duplicates.containsKey(entry.getValue())) {
                keys.add(entry.getKey());
                String val = duplicates.get(entry.getValue());
                if (!keys.contains(val)) {
                    keys.add(val);
                }
            } else {
                duplicates.put(entry.getValue(), entry.getKey());
            }
        }
        for(String key : keys) {
            map.remove(key);
        }
    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
