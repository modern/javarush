package com.javarush.test.level04.lesson16.home02;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());


        int[] mas = {a, b ,c};

        Arrays.sort(mas);
        System.out.println(mas[1]);
        /*int min, max;

        for(int i = 0; i < mas.length; i++) {
            min = mas[i];
            max = mas[i];
            if(min > mas[i]){ min = mas[i]; }
            if (max < mas[i]){ max = mas[i]; }
        }
        */

       /* for(int i = 0; i<mas.length; i++){
            if(mas[i] > mas[1] || mas[i] > mas[2] && mas[i] < mas[2] && mas[i] < mas[1]) {
                System.out.println(mas[i]);
            }
        }    */

        /*
        if(a > b || a > c && a < c && a < b) {
            System.out.println(a);
        }   */
    }
}
