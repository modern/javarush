package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String msg = reader.readLine();

        FileReader fr = new FileReader(msg);
        Scanner src = new Scanner(fr);

        ArrayList<Integer> num = new ArrayList<Integer>();

        while (src.hasNext()) {
            if(src.hasNextInt()) {
                int tmp = src.nextInt();
                if(tmp % 2 == 0) num.add(tmp);
            }
        }

        src.close();

        Collections.sort(num);

        for (Integer i : num) {
            System.out.println(i);
        }

    }
}
