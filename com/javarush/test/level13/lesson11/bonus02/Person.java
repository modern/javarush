package com.javarush.test.level13.lesson11.bonus02;

import java.util.ArrayList;
import java.util.List;

public class Person implements RepkaItem
{
    private String name;
    private String namePadezh;

    public Person(String name, String namePadezh)
    {
        this.name = name;
        this.namePadezh = namePadezh;
    }

    @Override
    public String getNamePadezh()
    {
        return this.namePadezh;         //*********
    }

    public void pull(Person person) {
        //List<Person> list = new ArrayList<Person>();

        System.out.println(name + " за " + person.getNamePadezh());
    }
}
