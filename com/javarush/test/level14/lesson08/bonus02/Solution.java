package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num1 = Integer.parseInt(reader.readLine());
        int num2 = Integer.parseInt(reader.readLine());
        int n = 1;

        NOD(num1, num2, n);
    }

    static void NOD(int num1, int num2, int n) {
        n = num1 % num2;
        num1 = num2;
        num2 = n;

        if (n > 0)
            NOD(num1, num2, n);
        else
            System.out.println(num1);
    }
}
