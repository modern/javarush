package com.javarush.test.level05.lesson12.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Задача по алгоритмам
Написать программу, которая:
1. вводит с консоли число N > 0
2. потом вводит N чисел с консоли
3. выводит на экран максимальное из введенных N чисел.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int maximum = 0;
        boolean flag = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        if(n <= 0){
            return;
        } else {
            for (int i = n; i != 0; i--) {
                int c = Integer.parseInt(reader.readLine());

                if(flag == true) {
                    maximum = c;
                    flag = false;
                } else if (c > maximum) {
                    maximum = c;
                }
            }
        }
        System.out.println(maximum);

        /*
        List<Integer> mas = new ArrayList<Integer>();
        boolean flag = true;
        int N = 0, show, maximum = 0;

        while(flag) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            N = Integer.parseInt(reader.readLine().trim());

            if(N <= 0){
                System.out.println("Число меньше 0 !");
            } else {
                show = N;
                flag = false;
            }
        }

        while(N != 0) {
            N--;
            int current = Integer.parseInt(reader.readLine().trim());
            mas.add(current);
        }

        maximum = mas.get(0);
        for (int i = 0; i < mas.size(); i++) {
            if(maximum < mas.get(i)) {
                maximum = mas.get(i);
                }
            }
        System.out.println(maximum);
        */
        }
}
