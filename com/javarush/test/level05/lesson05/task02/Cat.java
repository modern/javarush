package com.javarush.test.level05.lesson05.task02;

/* Реализовать метод fight
Реализовать метод boolean fight(Cat anotherCat):
реализовать механизм драки котов в зависимости от их веса, возраста и силы.
Зависимость придумать самому. Метод должен определять, выиграли ли мы (this) бой или нет,
т.е. возвращать true, если выиграли и false - если нет.
Должно выполняться условие:
если cat1.fight(cat2) = true , то cat2.fight(cat1) = false
*/

public class Cat
{
    public String name;
    public int age;
    public int weight;
    public int strength;

    public boolean win;

    public Cat()
    {
    }

    public boolean fight (Cat anotherCat)
    {
        if (anotherCat.weight > this.weight) {
            System.out.println("Выйграл " + anotherCat.name);
            win = false;
            return win;
        } else {
            System.out.println("Выйграл " + this.name);
            win = true;
            return win;
        }
    }

    public static void main(String[] args) {
        Cat tom = new Cat();
        Cat mike = new Cat();

        tom.weight = 10;
        tom.age = 10;
        tom.strength = 10;
        tom.name = "Tom";

        mike.weight = 9;
        mike.age = 10;
        mike.strength = 10;
        mike.name = "Mike";

        mike.fight(tom);
    }
}
