package com.javarush.test.level07.lesson04.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Массив из строчек в обратном порядке
1. Создать массив на 10 строчек.
2. Ввести с клавиатуры 8 строчек и сохранить их в массив.
3. Вывести содержимое всего массива (10 элементов) на экран в обратном порядке. Каждый элемент - с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] str = new String[10];
        int N = str.length;

        for (int i = 0; i < str.length-2; i++) {
            str[i] = reader.readLine();
        }

        for(int i = 0; i < N/2; i++) {
            String tmp = str[i];
            str[i] = str[N-1-i];
            str[N-i-1] = tmp;
        }

        for(int i = 0; i < str.length; i++) {
            System.out.println(str[i]);
        }

    }
}