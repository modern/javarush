package com.javarush.test.level07.lesson06.task01;

/* 5 различных строчек в списке
1. Создай список строк.
2. Добавь в него 5 различных строчек.
3. Выведи его размер на экран.
4. Используя цикл выведи его содержимое на экран, каждое значение с новой строки.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        ArrayList<String> str  = new ArrayList<String>();
        str.add("101");
        str.add("102");
        str.add("103");
        str.add("104");
        str.add("105");

        System.out.println(str.size());

        for(int i = 0; i < str.size(); i++) {
            System.out.println(str.get(i));
        }

    }
}
