package com.javarush.test.level07.lesson09.task05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Удвой слова
1. Введи с клавиатуры 10 слов в список строк.
2. Метод doubleValues должен удваивать слова по принципу a,b,c -> a,a,b,b,c,c.
3. Используя цикл for выведи результат на экран, каждое значение с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception {
        //read strings and init ArrayList list here - считать строки с консоли и объявить ArrayList list тут
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list  = new ArrayList<String>();

        for(int i = 0; i < 2; i++) {
            String n = reader.readLine();
            list.add(n);
        }

        ArrayList<String> result = doubleValues(list);

        for (int i = 0; i < result.size(); i++)
        {
            System.out.println(result.get(i));
        }
        //print result - вывести на экран result
    }

    public static ArrayList<String> doubleValues(ArrayList<String> list) {
        // add your code here - добавь код тут
        /*
        for (int i = 0; i < list.size(); i++) {
            String tmp = list.get(0) + list.get(0);
            list.remove(0);
            list.add(tmp);
        } */
        for (int i = 0; i < list.size(); i++)
        {
            list.add(i, list.get(i));
            i++;
        }

        return list;
    }
}
