package com.javarush.test.level07.lesson09.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* 5 слов в обратном порядке
Введи с клавиатуры 5 слов в список строк. Выведи их в обратном порядке.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> str  = new ArrayList<String>();

        for (int i = 0; i < 5; i++) {
            String s = reader.readLine();
            str.add(s);
        }

        /*
        Алгоритм для вывода ассоциативного массива строк в обратном порядке
         */
        for (int i = 0; i < str.size(); i++) {
            String tmp = str.get(i);
            str.remove(i);
            str.add(0, tmp);
        }

        for(int i = 0; i < str.size(); i++) {
            System.out.println(str.get(i));
        }
    }
}
