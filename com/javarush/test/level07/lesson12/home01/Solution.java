package com.javarush.test.level07.lesson12.home01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Вывести числа в обратном порядке
Ввести с клавиатуры 10 чисел и заполнить ими список.
Используя цикл for вывести их в обратном порядке.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> num  = new ArrayList<Integer>();

        for(int i = 0; i < 10; i++) {
            int n = Integer.parseInt(reader.readLine());
            num.add(n);
        }

        for (int i = 0; i < num.size(); i++) {
            int tmp = num.get(i);
            num.remove(i);
            num.add(0, tmp);
        }

        for(int i = 0; i < num.size(); i++) {
            System.out.println(num.get(i));
        }
    }
}
