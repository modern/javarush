package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        Human gF_one = new Human("Игорь", true, 66);
        System.out.println(gF_one.toString());

        Human gF_two = new Human("Дима", true, 67);
        System.out.println(gF_two.toString());

        Human gM_one = new Human("Лена", false, 66);
        System.out.println(gM_one.toString());

        Human gM_two = new Human("Яна", false, 67);
        System.out.println(gM_two.toString());

        Human father = new Human("Ашот", true, 32, gF_one, gM_one);
        System.out.println(father.toString());

        Human mother = new Human("Ана", false, 21, gF_two, gM_two);
        System.out.println(mother.toString());


        Human ch = new Human("Ана", false, 21, father, mother);
        System.out.println(ch.toString());

        Human ch1 = new Human("Ана", false, 21, father, mother);
        System.out.println(ch1.toString());

        Human ch2 = new Human("Ана", false, 21, father, mother);
        System.out.println(ch2.toString());


    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        Human (String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        Human (String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }
}
