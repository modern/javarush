package com.javarush.test.level07.lesson12.home02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Переставь три первых слова в конец списка
Ввести с клавиатуры 2 числа N  и M.
Ввести N строк и заполнить ими список.
Переставить M первых строк в конец списка.
Вывести список на экран, каждое значение с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        int N = Integer.parseInt(reader.readLine());
        int M = Integer.parseInt(reader.readLine());

        ArrayList<String> list = new ArrayList<String>();
        int count = 0;
        while (count != N)
        {
            String s = reader.readLine();
            list.add(s);
            count++;
        }

        for(int i = 0; i < M; i++) {
            String str = list.get(0);
            list.remove(0);
            list.add(str);
        }

        for (int i = 0; i < list.size(); i++)
        {
            System.out.println(list.get(i));
        }

        /*
        int[][] mas = new int[N][M];

        for(int i = 0; i < N; i++) {
            for(int j = 0; j < M; j++) {
                int tmp = Integer.parseInt(reader.readLine());
                mas[i][j] = tmp;
            }
        }

        for(int i = 0; i < N; i++) {
            for(int j = 0; j < M; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        */
    }
}
