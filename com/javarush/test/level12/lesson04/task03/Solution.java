package com.javarush.test.level12.lesson04.task03;

/* Пять методов print с разными параметрами
Написать пять методов print с разными параметрами.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static void print(int n) {

    }

    public static void print(Integer n) {

    }

    public static void print(char n) {

    }

    public static void print(byte n) {

    }

    public static void print(boolean n) {

    }

}
